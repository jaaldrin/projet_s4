# Projet  stm32 & mic PDM
### Jules-Arthur Aldrin, Antoine Madrelle, Thomas Bénéteau

# Sommaire

  - [Objectif du projet](#objectif-du-projet)
  - [Présentation du Hardware](#présentation-du-hardware)
    - [Carte STM32F429I-DISC1](#carte-stm32f429i-disc1)
    - [Adafruit PDM MEMS Microphone Breakout](#adafruit-pdm-mems-microphone-breakout)
  - [Prérequis](#prérequis)
    - [Utilisation de l'IDE & compilation du code](#utilisation-de-lide-compilation-du-code)
    - [Clignotement LED](#clignotement-led)
    - [Communication UART](#communication-uart)
    - [Fonctionnement du DMA](#fonctionnement-du-dma)
    - [Utilisation du DAC avec le DMA](#utilisation-du-dac-avec-le-dma)
    - [Microphone PDM](#microphone-pdm)
      - [Conversion PDM to PCM](#conversion-pdm-to-pcm)
  - [Réalisation du projet](#réalisation-du-projet)
    - [Configuration SAI & acquisition](#configuration-sai-acquisition)
    - [Fonction de conversion PDM to PCM](#fonction-de-conversion-pdm-to-pcm)
    - [Filtrage du signal audio](#filtrage-du-signal-audio)
    - [Principe général & implémentation du code](#principe-général-implémentation-du-code)

***

# Objectif du projet 

L'objectif de ce projet est d'enregistrer un son de quelques secondes sur la carte STM32 qui nous est fournie via un micro MEMS, puis de rejouer ce son sur des hauts-parleurs via le DAC. On privilégiera l'utilisation du DMA pour le transfert de données afin de décharger le CPU.

***
# Présentation du Hardware

## Carte STM32F429I-DISC1

La carte STM32F429ZI-DISC1 est une carte de développement basée sur le microcontrôleur STM32F429ZI de STMicroelectronics.

Elle dispose de nombreuses interfaces de communication, comme l'USB, CAN, UART, SPI, I2C et le SAI. 

Dans ce projet, on s'intéressera particulièrement au SAI (Serial Audio Interface) qui nous permettra de connecter des appareils audio numérique, ici le 'Adafruit PDM MEMS Microphone Breakout'.

On retrouve également 2 convertisseurs analogique-numérique 12 bits (ADC) avec une fréquence d'échantillonnage maximale de 2,4 MS/s et deux convertisseurs numérique-analogique (DAC) 12 bits avec une fréquence de conversion maximale de 2,1 MS/s. L'un de ce dernier nous servira dans le projet pour écouter le son enregistré.

![carte_stm32.png](Images/carte_stm32.png)

La carte est compatible avec les logiciels de développement  STM32CubeIDE (inclu avec STM32CubeMX pour gérer les paramètres des I/O, clock, etc), qui fournissent des outils pour le développement d'applications embarquées en C/C++ pour le microcontrôleur STM32.

***
## Adafruit PDM MEMS Microphone Breakout

Le microphone Adafruit PDM MEMS Microphone Breakout utilise la technologie MEMS  (Micro-Electro-Mechanical Systems). Les microphones MEMS utilisent de minuscules structures mécaniques et électriques sur une puce de silicium pour capturer le son. Cette technologie offre plusieurs avantages, notamment une taille compacte, une faible consommation d'énergie.

Il utilise la modulation PDM (Pulse Density Modulation) pour convertir le signal audio en un flux numérique en modulant la densité d'impulsions. Nous devrons donc procéder à une conversion PDM vers PCM pour pouvoir utiliser le DAC de la carte stm32 si on souhaite lire l'audio enregistré.

![micro_PDM.png](Images/micro_PDM.png)


***
# Prérequis 

Pour mener à bien ce projet il nécessaire de maîtriser certains aspects de notre environnement de travail (STM32CubeIDE) et de savoir utiliser certaines fonctionnalitées importantes de la carte. Certains concept, comme le DMA sont également important à comprendre, pour savoir pourquoi c'est utile de les utilisés.


## Utilisation de l'IDE & compilation du code

L'interface STM32CubeIDE fournit par STMicroelectronics permet de tout faire en un. 
C'est est un environnement de développement intégré (IDE) conçu spécifiquement pour les microcontrôleurs STM32 de STMicroelectronics. Il offre un ensemble d'outils pour le développement d'applications embarquées, permettant de coder, déboguer et déployer des projets relativement rapidement.

![stm32CubeIDE](Images/IDE1.png)

Cet IDE repose sur Eclipse, une plateforme de développement largement utilisée dans l'industrie, et intègre des fonctionnalités spécifiques aux microcontrôleurs STM32, telles que la génération de code HAL (Hardware Abstraction Layer) et la configuration des périphériques via STM32CubeMX.

Il est donc nécessaire d'installer cet IDE et de mettre en fonctionnement la chaine de compilation et de téléversement du code sur la carte. 

STM32CubeMX étant maintenant integré à l'IDE il n'est plus nécessaire de le télécharger en plus. On peut donc configurer tous les ports I/O, les timers, clock, etc, de manière graphique sur l'IDE et lui demander ensuite de générer le code nécessaire au paramétrage effectué.

![stm32CubeMX](Images/cubeMX.png)


## Clignotement LED

Le "helloworld" du monde de l'embarqué, faire clignoter une led dans un premier temps nous permet de prendre un main facilement la carte STM32 et de nous assurer que la chaîne de compilation et de téléversement fonctionne correctement. 

Pour ça, on utilise une sortie du microcontroleur déjà mise en output et qui est également déjà branchée à une led. 
Lorsqu'on créer un projet par défaut, la pin PG13, relier à la led L3 est directement configurer en output. On peut le vérifier sur l'interface cubeMX dans l'IDE comme ci-dessous:

![clignotement_led.png](Images/clignotement_led.png)

Si la pin n'était pas configuré en output, on peut a nouveau générer le code. (A la sauvegarde l'IDE le propose automatiquement), puis écrire le code suivant dans la boucle while(1):

```
/* USER CODE BEGIN WHILE */
  while (1)
  {
	  HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_13);
	  HAL_Delay(1000);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}
```


## Communication UART

Dans le même projet que précédemment on pourra configurer le fichier ioc comme ci-dessous pour mettre en place une communication UART.

Dans "connectivity" on pourra configurer "UART1" comme ci-dessous: 

![config_uart.png](Images/config_uart.png)

On vérifie également que les broches PA10 et PA9 sont en mode USART1_RX et USART1_TX, comme indiqué dans l'image ci-dessous :

![pin_uart.png](Images/pin_uart.png)

Une fois la configuration faite, on génère à nouveau le code et on pourra écrire le code suivant:

```
  /* USER CODE BEGIN 2 */
  uint8_t tx_buffer[34] = "Bonjour Jules-Arthur Aldrin !\n\r"; /
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    HAL_UART_Transmit(&huart1, tx_buffer, 34, 10);
	HAL_Delay(500);
  }
  /* USER CODE END 3 */
```

## Fonctionnement du DMA

Le DMA (Direct Access Memory) est un mécanisme permettant une communication directe entre la mémoire vive et certains périphériques sans passer par le processeur. Il permet de décharger le CPU, et on pourra donc l'utiliser pour d'autres choses. (Dans notre cas des calculs de conversion PDM - PCM par exemple)

![Sans DMA](Images/dma1.png)

![Avec DMA](Images/dma2.png)

D'autres mécanisme sont liés au DMA. C'est le cas des fonctions de callback. Elles sont utilisées pour signaler à un programme qu'une partie de la mémoire tampon (buffer) a été lue ou écrite. Elles sont utiles car elles permettent au programme de connaître l'état du transfert DMA en temps réel et de réagir en conséquence.

Un buffer ping-pong permet d'optimiser les transferts de données. La technique consiste à utiliser deux buffers plutôt qu'un seul. Pendant que le DMA lit ou écrit des données dans un buffer, le programme peut utiliser l'autre buffer pour traiter les données précédemment transférées. Une fois le transfert terminé, les rôles des deux buffers sont inversés. Cette technique permet de réduire les temps d'attente et d'améliorer l'efficacité des transferts.

_(Source : https://www.digikey.fr/en/maker/projects/getting-started-with-stm32-working-with-adc-and-dma/f5009db3a3ed4370acaf545a3370c30c)_


## Utilisation du DAC avec le DMA
    Antoine maïster

## Microphone PDM

L’objectif de cette section est de comprendre le fonctionnement de la modulation PDM (Pulse Density Modulation), afin d’être capable d’exploiter les données renvoyées par le microphone.

Il s’agit d’une modulation, qui nous renvoie un signal binaire. Les bits sont encodés à ‘0’ ou à ‘1’, en fonction de l’amplitude du point d’échantillonnage précédent.

Pour comprendre le fonctionnement de ce type de modulation, nous avons simulé son fonctionnement à l’aide de python. L’objectif est dans un premier temps de générer une sinusoïde, et de la convertir en PDM. Pour effectuer ce travail, les modules « numpy » et « matplotlib » ont été utilisés pour générer et visualiser les courbes souhaitées. 

![Sinusoïde_intiale.png](Images/Sinusoïde_intiale.png)

La conversion de cette sinusoïde est faite à l’aide de la fonction python suivante :

![Code_python.png](Images/Code_python.png)

Après avoir traité la sinusoïde avec le code présent ci-dessus, nous obtenons le signal suivant modulé en PDM. À noter que sur l’exemple présent ci-dessous, le modulation à été faite à une fréquence d’échantillonnage trop faible pour être exploitée. Cela nous permet de bien visualiser le fonctionnement de ce traitement, mais pour la suite des simulations, nous allons réhausser cette fréquence.

![Sinusoïde_module_pdm.png](Images/Sinusoïde_module_pdm.png)

***
### Conversion PDM to PCM

Afin de pouvoir être utilisé par le DAC, les données PDM doivent être converties en PCM (Pulse Code Modulation). 
Avant d’expliquer la conversion de PDM à PCM, nous allons faire un point sur ce qu’est la modulation PCM. 
 
Le principe ici, est de prendre un signal, de l’échantillonner, et de garder la valeur du signal à l’instant t de chaque échantillon. 

Si l’on reprend la même sinusoïde que tout à l’heure et que nous la modulons en PCM, nous obtenons le résultat suivant. A noter que la fréquence d’échantillonnage est bien trop faible, cela permet juste d’imager le principe de la modulation.

![Sinusoïde_pcm.png](Images/Sinusoïde_pcm.png)


L’objectif suivant été de convertir des données PDM en données PCM. Pour ce faire, nous prenons les bits de la conversion PDM par groupe de 64. Nous obtenons donc des buffer sous la forme suivante:

![buffer_64.png](Images/Exemple_buffer.png)

Pour obtenir la valeur en PCM, nous devons additionner tous les bits à ‘1’ dans le buffer. Le résultat de cette addition nous donne la valeur du point correspondant en PCM. Le processus est ensuite à répéter pour les 64 bits suivant, et ainsi de suite jusqu’à avoir traité toutes les valeurs du buffer PDM. 

Si nous reprenons l’exemple de la sinusoïde, et que nous augmentons la fréquence d’échantillonnage, nous pouvons obtenir un résultat satisfaisant à l’aide du code python suivant :

![code_convert.png](Images/Code_python_pdm_to_pcm.png)

![rendu_final.png](Images/pdm_to_pcm.png)

L’implémentation de ce petit programme nous a permis de nous imprégner des différents types de modulation, et de comprendre comment elles fonctionnaient.

***
# Réalisation du projet

## Configuration SAI & acquisition

L'interface SAI (Serial Audio Interface) STM32 est une composante intégrée dans notre STM32. Elle permet de gérer des flux audio numériques, permettant la transmission bidirectionnelle de données audio entre le microcontrôleur et les périphériques audio externes. 

Dans notre cas, on devra configurer notre SAI en réception, puisqu'on utilise un microphone branché sur notre carte. 

En étudiant la documentation "Interfacing PDM digital microphones using STM32 MCUs and MPUs" on configure le SAI, comme ci-dessous:

![config_sai1.png](Images/config_sai1.png)

Comme dit précédemment, c'est important de configurer "audio mode" en "Master receive" (contrairement à ce qui est montré dans la documentation) afin de pouvoir paramétrer la direction du stream en "peripheral to memory" pour utiliser le DMA du SAI vers un buffer. (cf image ci-dessous)

![config_sai1.png](Images/config_sai2.png)

On a également configurer la fréquence audio à 48KHz et la fréquence de notre clock pour faire fonctionner le microphone à 3.072 MHz. 



***

## Fonction de conversion PDM to PCM

Code de la fonction

Quels ont été les problèmes rencontrés ?

***

## Filtrage du signal audio

Le signal audio n'est malheureusement pas audible dès la conversion en PCM et il est nécessaire de procéder à un filtrage avant l'écoute. 

Ce traitement sert à l'élimination du bruit : Le signal PDM peut être sensible aux interférences électromagnétiques et au bruit. (Typiquement celles provenant des alimentations électriques des bâtiments. 50Hz en europe).
Lorqu'on convertit le signal en PCM, ces bruits peuvent subsister et rendre le signal audio très parasité. Un filtrage approprié permet d'atténuer ces artefacts et de produire un signal audio plus propre.

EXPLICATION ET SCREEN DU FILTRAGE MATLAB + implementation C

***

## Principe général & implémentation du code

***